package src.web;

import java.awt.AWTException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class launch {
	public static void main(String[] args) throws InterruptedException, IOException, AWTException
	{WebDriver wd;
	  System.setProperty("webdriver.chrome.driver","D:\\Automation Testing\\Tools\\chromedriver.exe");
	  wd= new ChromeDriver();
      wd.get("http://money.rediff.com/gainers/bsc/dailygroupa?");         
      //No.of Columns
      List  col = wd.findElements(By.xpath(".//*[@id=\"leftcontainer\"]/table/thead/tr/th"));
      System.out.println("No of cols are : " +col.size()); 
      //No.of rows 
      List  rows = wd.findElements(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr/td[1]")); 
      System.out.println("No of rows are : " + rows.size());
      //wd.close();
      
      WebElement table = wd.findElement(By.tagName("table")) ;
      WebElement tablerow= table.findElement(By.xpath("//*[@id='leftcontainer']/table/tbody/tr[513]"));
      String rowtext = tablerow.getText();
      System.out.println("row text is :" +rowtext);
      
      WebElement cell = wd.findElement(By.xpath("//*[@id=\"leftcontainer\"]/table/tbody/tr[513]/td[4]"));
      String celldata = cell.getText();
      System.out.println("cell data is : " +celldata );
      
}
}
